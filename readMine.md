# 2017-10-23

## git操作
<details>
	<summary>git基本操作</summary>
##### 1. Git全局设置

git config --global user.name "耍帅的兔子"

git config --global user.email "1578779013@qq.com"

Create a new repository

git clone https://gitlab.com/BowenTH/note.git

cd note

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master

##### 2. 导入已经存在的文件夹

cd existing_folder

git init

git remote add origin https://gitlab.com/BowenTH/note.git

git add .

git commit -m "Initial commit"

git push -u origin master
##### 3. 导入已存在的Git库
cd existing_repo

git remote add origin https://gitlab.com/BowenTH/note.git

git push -u origin --all

git push -u origin --tags
</details>

## Markdown特殊标记

1. 详情展开
<details>
	<summary>点击展开</summary>
	你好，你看到我了
	<code>alert("hello")<code>
	<pre>代码块</pre>
</details>
2. 表格

|头一有对齐方式哟|头二对头居中的这个是|
| :-------- | :---: |
|列一|列二|
|行一|行二|
3. 链接
> `![图片](img.png)`
`![图片][code]`
`[code]:img.png`

You can add footnotes to your text as follows.[^2]
[^2]: This is my awesome footnote.
